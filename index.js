
function takeInput() { 
    let input = document.getElementById('inputBox').value.toUpperCase();

    let found = false;

    for(let x=0; x<families.length; x++) {
        if(families[x].value.indexOf(input) != -1) {
            
            found = true;

            let insts = families[x].instruments;

            let instInfo = families[x].title;

            let makeElement = document.getElementById('familyHold');
            makeElement.innerHTML = instInfo;

            instList = "";
            for(var propName in insts) {
                let inst = insts[propName];
                instList += "<li>" + inst.title + "</li>\n";
            }

            let instElement = document.getElementById('instrumentsHold');
            instElement.innerHTML = instList;

            break;
        }
    };

    if(found == false) {
        alert("The family '" + document.getElementById('inputBox').value + "' is not in the database.");
    }
};

families = [
    {
        "value": "BRASS",
        "title": "Brass",
        "instruments": [
            {
                "value": "TRUMP",
                "title": "Trumpet"
            },
            {
                "value": "TROM",
                "title": "Trombone"
            },
            {
                "value": "HORN",
                "title": "Horn in F"
            },
            {
                "value": "BARITONE",
                "title": "Baritone"
            },
            {
                "value": "EUPH",
                "title": "Euphonium"
            },
            {
                "value": "TUBA",
                "title": "Tuba"
            }
        ]
    },
    {
        "value": "WOODWIND",
        "title": "Woodwind",
        "instruments": [
            {
                "value": "FLUTE",
                "title": "Flute",
            },
            {
                "value": "CLAR",
                "title": "Clarinet"
            },
            {
                "value": "OBOE",
                "title": "Oboe"
            },
            {
                "value": "BASSN",
                "title": "Bassoon"
            },
            {
                "value": "ENGLISH",
                "title": "English Horn"
            },
            {
                "value": "SOPXS",
                "title": "Soprano Saxophone"
            },
            {
                "value": "ALTOSX",
                "title": "Alto Saxophone"
            },
            {
                "value": "TENORSX",
                "title": "Tenor Saxophone"
            },
            {
                "value": "BARISX",
                "title": "Baritone Saxophone"
            }
        ]
    },
    {
        "value": "STRINGS",
        "title": "Strings",
        "instruments": [
            {
                "value": "VIOLIN",
                "title": "Violin"
            },
            {
                "value": "VIOLA",
                "title": "Viola"
            },
            {
                "value": "CELLO",
                "title": "Cello"
            },
            {
                "value": "DBLBASS",
                "title": "Double Bass"
            }
        ]
    },
    {
        "value": "PERCUSSION",
        "title": "Percussion",
        "instruments": [
            {
                "value": "SNARE",
                "title": "Snare Drum"
            },
            {
                "value": "BASS",
                "title": "Bass Drum"
            },
            {
                "value": "CYMB",
                "title": "Cymbals"
            },
            {
                "value": "TAMB",
                "tile": "Tambourine"
            },
            {
                "value": "GLOCK",
                "title": "Glockenspiel"
            },
            {
                "value": "XYLO",
                "title": "Xylophone"
            },
            {
                "value": "MARIM",
                "title": "Marimba"
            },
            {
                "value": "CHIME",
                "title": "Chimes"
            },
            {
                "value": "TYMP",
                "title": "Tympani"
            }
        ]
    }
]